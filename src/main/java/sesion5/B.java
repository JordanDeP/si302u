package sesion5;

public class B extends A{
    private String nombre;
    private Integer tamano;
    public B(){
        super("Segura");
    }
    public B(String nombre){
        this.nombre=nombre;
    }
    public static void main(String[] args) {
        B s=new B();
        B s2= new B("Luis");
        System.out.println(s.nombre);
        System.out.println(s2.nombre);
        s2.agregar("1");
        s2.agregar("1","2");
        s2.agregar("1","2","3");
        s2.agregar();
        s2.pintar();
        A a=new A();
        a.pintar();
        A nuevo=s2;
        B b1=(B) nuevo;
        nuevo.pintar();

    }

    public void agregar(String a){
        System.out.println(a);
        System.out.println("1 parametro");
    }
    public void agregar(String a,String b){
        System.out.println(a);
        System.out.println(b);
        System.out.println("2 parametros");
    }
    public void agregar(Integer a){
        System.out.println(a);
    }
    public void agregar(String... a){  //mínima prioridad, solo va 1 ,siempre va al final
        //System.out.println(a[0]);
        System.out.println("argumentos variables");
    }
    public void pintar(){ //PRIMERO B, luego va al padre
        System.out.println("Aprendi a bailar en SALSUNI, sin pareja");
    }
}
